function W = selectFeats(Win,Feats)

dims = size(Win);
W = Win;

for h = 1:dims(1)
    for i = 1:dims(2)
        if ~isempty(Win{h,i})
            W{h,i}.D = Win{h,i}.D(:,Feats);   
            temp = sum(isnan(W{h,i}.D),2);
            temp = temp==0;
            
            W{h,i}.D(:,11) = 2./W{h,i}.D(:,11);
            
            W{h,i}.D = W{h,i}.D(temp,:);
        end
    end
end
