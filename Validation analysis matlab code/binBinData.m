function [Sbin, Wbin] = binBinData(Smat,W)

if ~iscell(Smat);
    
    dims = size(Smat);
    Wbin = cell(1,numel(W));
    
    Smeans = mean(Smat);
    
    Smeans1 = repmat(Smeans,dims(1),1);
    Sbin = (Smat>Smeans1);
    
    for i = 1:numel(W)
        rows = size(W{i}.D,1);
        means = repmat(Smeans,rows,1);
        temp.D = (W{i}.D>means);
        Wbin{i} = temp;
    end
    
elseif iscell(Smat)
    
    Wbin = cell(size(W));
    Sbin = cell(size(Smat));
    for h = 1:numel(Smat)
        tempSmat = Smat{h};
        dims = size(tempSmat);
        
        Smeans = mean(tempSmat);
        Smeans = Smeans;
        
        Smeans1 = repmat(Smeans,dims(1),1);
        Sbin{h} = (tempSmat>Smeans1);
        
        for i = 1:size(W,2)
            if ~isempty(W{h,i})
                
                rows = size(W{h,i}.D,1);
                means = repmat(Smeans,rows,1);
                temp.D = 1.0*(W{h,i}.D>(means));
                Wbin{h,i} = temp;
            end
        end
    end
    
end

