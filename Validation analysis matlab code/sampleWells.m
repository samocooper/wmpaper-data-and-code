function Smat = sampleWells(W,n)

if size(W,1) == 1
    
    Snum = size(W,2);
    Ssize = round(n/Snum);
    
    S = cell(Snum,1);
    
    for i = 1:Snum
        if size(W{1,i}.D,1)>Ssize
            R = randperm(size(W{1,i}.D,1));
            S{i} = W{1,i}.D(R(1:Ssize),:);
        elseif size(W{1,i}.D,1) > 10
            S{i} = W{1,i}.D;
        end
    end
    
    Smat = cell2mat(S);
    
else
    for h = 1:size(W,1)
        Snum = size(W,2);
        Ssize = round(n/Snum);
        
        S = cell(Snum,1);
        
        for i = 1:Snum
            if ~isempty(W{h,i})
                if size(W{h,i}.D,1)>Ssize
                    R = randperm(size(W{h,i}.D,1));
                    S{i} = W{h,i}.D(R(1:Ssize),:);
                elseif size(W{h,i}.D,1) > 10
                    S{i} = W{h,i}.D;
                end
            end
        end
        
        Smat{h} = cell2mat(S);
    end
    
    
end

