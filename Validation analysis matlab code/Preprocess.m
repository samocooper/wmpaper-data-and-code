function [W,Smats] = Preprocess(Win,Smats)

dims = size(Win);
W = Win;

for h = 1:dims(1)
    [Smats{h},mu,sigma] = zscore(Smats{h});
    
    stemp1 = Smats{h} < -4;
    stemp2 = Smats{h} > 4;
    stemp3 = stemp1 + stemp2;
    stemp = sum(stemp3,2);
    stemp = stemp==0;
    Smats{h} = Smats{h}(stemp,:);
    
    for i = 1:dims(2)
        if ~isempty(Win{h,i})
            muMat = repmat(mu,size(Win{h,i}.D,1),1);
            sigMat = repmat(sigma,size(Win{h,i}.D,1),1);
            W{h,i}.D = (Win{h,i}.D-muMat)./sigMat;
            
            temp1 = W{h,i}.D < -4;
            temp2 = W{h,i}.D > 4;
            temp3 = temp1 + temp2;
            temp = sum(temp3,2);
            temp = temp==0;
            W{h,i}.D = W{h,i}.D(temp,:);
             
            if  size(W{h,i}.D)<20;
                 W{h,i} = [];
            end
        end
    end
end
