function [W,Feat] = fileLoad(Fdir)

PKdir = strcat(Fdir,'/Plate_key.txt');
PK = readtable(PKdir,'Delimiter','\t');
Pname = PK{:,1};
Pname = unique(Pname);

W = {};
for i = 1:numel(Pname);    
    Fname = strcat(Fdir,'/',Pname{i});    
    D = PK{strcmp(PK{:,1},Pname{i}),2:3};
    
    TCnames = PK{strcmp(PK{:,1},Pname{i}),4};
    Catnames = PK{strcmp(PK{:,1},Pname{i}),5};

    for j = 1:size(D,1)
        try
            temp.Fname = strcat(Fname,'/',num2str(D(j,1),'%#0.3i'),num2str(D(j,2),'%#0.3i'),'000.txt');
            temp.Pname = Pname;
            temp.TCname = TCnames{j};
            temp.Catname = Catnames{j};
            Dtemp = dlmread(temp.Fname,'\t',1,3);
            Dnan = sum(Dtemp,2);
            Dtemp(isnan(Dnan),:) = [];
            temp.D = Dtemp;
            W{i,j} = temp;
        catch
            msg = strcat(Fname,'/',num2str(D(j,1),'%#0.3i'),num2str(D(j,2),'%#0.3i'),'000.txt');
            %disp(msg);
        end
    end
end

Featdir = strcat(Fdir,'/Features.csv');
Feattab = readtable(Featdir);
Feat = Feattab{:,1};