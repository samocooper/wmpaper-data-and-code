%% 
% generate results for Validation, all matrices need to be loaded
[W,F] = fileLoad('Single_cell_data_1');
Wnum = numel(W);


W = selectFeats(W,[21 22 33 32 31 30 29 28 27 26 34 24 25 14 2]);

Smats  = sampleWells(W,6000);
[W, Smats] = Preprocess(W,Smats);

%%
[Sbin,Wbin] = binBinData(Smats,W);


%% test for plate effects

PInd = cell(1,numel(Smats));

for i = 1:numel(Smats)
    
    [~,a]  = pdist2(Cbest,Sbin{i},'euclidean','Smallest',1);    
    temp = zeros(1,6);
    for j = 1:6
        temp(j) = sum(a==j);
    end
    temp = temp./sum(temp);
    PInd{i} = temp;
    
end

PInd = cell2mat(PInd);
PInd = reshape(PInd,6,[])';
imagesc(PInd)

%%  Plate clustering

dims = size(W);

Ind = cell(dims);
for h = 1:dims(1)
    for i = 1:dims(2)
        if ~ isempty(Wbin{h,i})
            [~,a]  = pdist2(Cbest,Wbin{h,i}.D,'euclidean','Smallest',1);
            temp = zeros(1,6);
            for j = 1:6
                temp(j) = sum(a==j);
            end
            temp = temp./sum(temp);
            temp = temp - PInd(h,:);
            Ind{h,i} = temp;
        end
    end
end

%
names = cell(dims);
cat = cell(dims);

for h = 1:dims(1)
    for i = 1:dims(2)
        if ~ isempty(W{h,i})
            names{h,i} = W{h,i}.TCname;
            cat{h,i} = W{h,i}.Catname;
        end
    end
end

namesVec = reshape(names,1,[]);
IndVec = reshape(Ind,1,[]);
catVec = reshape(cat,1,[]);

%%

All_KD = cell2mat(IndVec);
All_KD = reshape(All_KD,6,[]);
All_KD  = All_KD';

%%
%{
namesU = {};
for i = 1:numel(namesVec)
    if ~isempty(namesVec{i})
        namesU{end+1} = namesVec{i};
    end
end
namesU = unique(namesU);
%}
%%
namesU = {'NT','RAC2','CDC42','RHOJ','RND1','RAC1','RHOU','RHOA','RHOF','RHOG','RND2','RHOC','RHOB','RND3','RAC3','RHOH','RHOD'};
%%
bins = 0.2;
URall = [];
UR1all = {};
for h = 1:200
    RnamesU = namesU(randperm(numel(namesU)));    
    UR = {};
    for i = 1:numel(namesU)
        
        gene = RnamesU{i};
        
        Validate = IndVec(1,strcmp(namesVec,gene));
        Validate = cell2mat(Validate);
        Validate = reshape(Validate,6,[])';    
        
        
        siRNA = unique(catVec(strcmp(namesVec,gene)));
        KDmean = [];
        for j= 1:numel(siRNA)        
            KD = IndVec(1,strcmp(catVec,siRNA(j)) & strcmp(namesVec,gene));
            KD = cell2mat(KD);
            KD = reshape(KD,6,[])';
            KDmean(j,:) = mean(KD);
            KDmean(j,:) = KDmean(j,randperm(6));
        end
        
        temp = corr(DmeanN(i,:)',KDmean','type','Pearson');
        UR{i} = temp;
    end
    UR1 = cell2mat(UR);    
    URall(h,:) = histc(UR1,-1:bins:1);
    UR1all{h} = UR1;
end
URallM = mean(URall);
subplot(2,1,1)
UR1allMat = cell2mat(UR1all);
disp(strcat('x.Pval--mean : ',num2str(mean(UR1allMat)),'      --stdev : ',num2str(std(UR1allMat))));

hist(UR1allMat);
%%
Cdist = sort(UR1allMat);
%bar(URallM)

%%
Cdist1 = [Cdist,1];
Cdist2 = [-1,Cdist];
UP = [];

U = {};
X = {};
for i = 1:numel(namesU)
    gene = namesU{i};
    Validate = IndVec(1,strcmp(namesVec,gene));
    Validate = cell2mat(Validate);
    Validate = reshape(Validate,6,[])';        
  
    siRNA = unique(catVec(strcmp(namesVec,gene)));
    
    KDmean = [];
    for j= 1:numel(siRNA)        
        KD = IndVec(1,strcmp(catVec,siRNA(j)) & strcmp(namesVec,gene));
        KD = cell2mat(KD);
        KD = reshape(KD,6,[])';
        KDmean(j,:) = mean(KD);
    end
    
    %temp = pdist2(DmeanN(i,:),Validate,'Euclidean');
    
    temp = corr(DmeanN(i,:)',Validate','type','Pearson');
    
    
    temp2 = corr(DmeanN(i,:)',KDmean','type','Pearson');    
    U{i} = temp2;
    for j = 1:numel(temp2)
        x.Pval = find(Cdist2<temp2(j) & temp2(j)<Cdist1)/numel(Cdist);
        x.si = siRNA{j};
        x.name = gene;
        x.dist = KDmean(j,:);
        
        if x.Pval>0.999            
            x.sig = '***';
        elseif x.Pval>0.99
            
            x.sig = '**';
        elseif x.Pval>0.95
            
            x.sig = '*';
        elseif x.Pval>0.9

            x.sig = '"';
        else
            x.sig = '';
        end       
        X{i,j} = x;
    end
end
U1 = cell2mat(U);
UallM = histc(U1,-1:bins:1);
disp(strcat('--mean : ',num2str(mean(U1)),'      --stdev : ',num2str(std(U1))));

subplot(2,1,2)
hist(U1)
%%
%bar(UallM)
%bar((-1:bins:1)+0.1,UallM-URallM)
%[~,p] = kstest2(UR1allMat,U1);
a = 0.967
find(Cdist2<a & a<Cdist1)/numel(Cdist)
%%
figure
for i = 1:size(X,1)
    
    gene = X{i,1}.name;
   
    Z = [];
    L = {};
    for j = 1:size(X,2)
        if ~isempty(X{i,j})
            Z(j,:) = X{i,j}.dist;
            L{j} = [X{i,j}.sig ,' ',X{i,j}.si(end-1:end)];
        end
    end
    
    Z(end+1,:) = -0.3;
    Z(end+1,:) =  DmeanN(i,:);
    Z(end+1,1) = 0.3;
    Z(end,2) = -0.3;
    L{end+2} = 'siPool';
    subplot(4,5,i)
    imagesc(Z);
    set(gca,'YTickLabel',L,'YTick',1:numel(L),'Ylim',[0.5 size(Z,1)-0.5],'FontSize',8)
    
    title(gene)
end
