function dist = pdistraw(V,M,Count)

    dist = 0;

    for i = 1:Count
        D = (V - M(i,:)).^2;
        dist = dist + sqrt(sum(D));
    end

end