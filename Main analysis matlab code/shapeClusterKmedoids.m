function cost = shapeClusterKmedoids(arg)

    Smat = arg{1};
    TCs = arg{2};
    TCsize = arg{3};
    GI = arg{4};
    RC = arg{5};    
    k = arg{6};
    
    [~,C] = kmedoids(Smat,k,'MaxIter',200,'emptyaction','singleton','Replicates',10);
    
    Dshape = zeros(numel(GI),k);

    for i = 1:numel(GI)
        
        [~,index_tc] = min(pdist2(TCs{i},C),[],2);

        for j = 1:k
            Dshape(i,j) = sum(index_tc == j);
        end
    end
    
    Dnorm = Dshape./TCsize;
    cost = DBcost(Dnorm,GI,RC,size(Dnorm,2));
end