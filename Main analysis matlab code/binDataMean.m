function [Sbin,TCbin] = binDataMean(Smat,TCs)

dims = size(Smat);
TCbin = {};

Smeans = mean(Smat);

Smeans1 = repmat(Smeans,dims(1),1);
Sbin = 1.0*(Smat>Smeans1);

for i = 1:numel(TCs)
    rows = size(TCs{i},1);
    temp = repmat(Smeans,rows,1);
    
    TCbin{i} = 1.0*(TCs{i}>temp);
end

end

