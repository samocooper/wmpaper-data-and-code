function cost = DBcost(DM,GI,RC)

g = max(GI);
C = zeros(g,size(DM,2));
S = zeros(1,g);

for i = 1:g
        temp = DM(GI == i,:);
        C(i,:) = sum(temp);
end
     
reps = repmat(RC',1,size(C,2)); % mean profiled poorly, so using stored mat
C = C./reps;

for i = 1:g
        temp = DM(GI == i,:);
        s = sum(pdistraw(C(i,:),temp,RC(i)));
        S(i) = s;
end

S = S./RC;
Cd = squareform(pdist(C));
Cd(Cd < 0.01) = 0.01;

cost = daviesBouldin(Cd,S);
end