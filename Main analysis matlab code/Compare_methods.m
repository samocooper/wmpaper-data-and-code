% Script for performing comparison of data transformation and clustering
% methods

%% Load Wells

Samp = 400;
indsX = [];

[~, TCis, ~, GI, RC, gene_names] = cross_validate('tracked_cells1',2000,indsX,Samp);

KmeansU = zeros(100,7);
GMMmeansU = zeros(100,7);
CmeansU = zeros(100,7);
HeirmeansU = zeros(100,7);

% Mean DBI for shape clustering methods with continuous features over 100 resamplings

for k = 1:7
    for i = 1:100
        i
        [Smat,TCs] = sample(TCis,300,2000);

        TCnum = numel(TCs);
        Feats = size(TCs{1},2);

        arg = {Smat,TCs,300,GI,RC,k};
        
        KmeansU(i,k) = shapeClusterKmeans(arg);
        try
        GMMmeansU(i,k) = shapeClusterGMM(arg);
        catch
        end
        CmeansU(i,k) = shapeClusterCmeans(arg);
        HeirmeansU(i,k) = shapeClusterHeirarchichal(arg);
       
    end
end
%%

hold on

plot(1:7,nanstd(PCMeanU'),'Color',[0.9 0 0 ])
plot(1:7,nanstd(KmeansU),'Color',[0 0.9 0 ])
plot(1:7,nanstd(GMMmeansU),'Color',[0 0 0.9])
plot(1:7,nanstd(CmeansU),'Color',[0.9 0 0.9])
plot(1:7,nanstd(HeirmeansU),'Color',[0 0.9 0.9])

hold off
%%

hold on

plot(1:7,median(PCMeanU'),'Color',[0.9 0 0 ])
plot(1:7,median(KmeansU),'Color',[0 0.9 0 ])
plot(1:7,median(GMMmeansU),'Color',[0 0 0.9])
plot(1:7,median(CmeansU),'Color',[0.9 0 0.9])
plot(1:7,median(HeirmeansU),'Color',[0 0.9 0.9])

hold off

%%

hold on

plot(1:7,nanmean(PCMeanU'),'Color',[0.9 0 0 ])
plot(1:7,nanmean(KmeansU),'Color',[0 0.9 0 ])
plot(1:7,nanmean(GMMmeansU),'Color',[0 0 0.9])
plot(1:7,nanmean(CmeansU),'Color',[0.9 0 0.9])
plot(1:7,nanmean(HeirmeansU),'Color',[0 0.9 0.9])

hold off
%%

%% PCA transform data then test methods


%% Load Wells

Samp = 400;
indsX = [];

[~, TCis, ~, GI, RC, gene_names] = cross_validate('tracked_cells1',2000,indsX,Samp);

U = zeros(7,100);

KmeansU = zeros(100,7);
GMMmeansU = zeros(100,7);
CmeansU = zeros(100,7);
HeirmeansU = zeros(100,7);

% Mean DBI for shape clustering methods with continuous features over 100 resamplings

for k = 1:7
    for i = 1:100
        i
        [Smat,TCs] = sample(TCis,300,2000);

        TCnum = numel(TCs);
        Feats = size(TCs{1},2);
        
        [W,Smat] = princomp(Smat);
        
        for j = 1:numel(TCs)
            temp = TCs{j}*W';
            TCs{j} = temp(:,1:2);
        end

        arg = {Smat(:,1:2),TCs,300,GI,RC,k};
        
        KmeansU(i,k) = shapeClusterKmeans(arg);
        try
        GMMmeansU(i,k) = shapeClusterGMM(arg);
        catch
        end
        CmeansU(i,k) = shapeClusterCmeans(arg);
        HeirmeansU(i,k) = shapeClusterHeirarchichal(arg);
       
    end
end

%%
hold on

plot(1:7,nanmean(PCMeanU'),'Color',[0.9 0 0 ])
plot(1:7,nanmean(KmeansU),'Color',[0 0.9 0 ])
plot(1:7,nanmean(GMMmeansU),'Color',[0 0 0.9])
plot(1:7,nanmean(CmeansU),'Color',[0.9 0 0.9])
plot(1:7,nanmean(HeirmeansU),'Color',[0 0.9 0.9])

hold off
%%

hold on

plot(1:7,median(PCMeanU'),'Color',[0.9 0 0 ])
plot(1:7,median(KmeansU),'Color',[0 0.9 0 ])
plot(1:7,median(GMMmeansU),'Color',[0 0 0.9])
plot(1:7,median(CmeansU),'Color',[0.9 0 0.9])
plot(1:7,median(HeirmeansU),'Color',[0 0.9 0.9])

hold off

%%

hold on

plot(1:7,nanstd(PCMeanU'),'Color',[0.9 0 0 ])
plot(1:7,nanstd(KmeansU),'Color',[0 0.9 0 ])
plot(1:7,nanstd(GMMmeansU),'Color',[0 0 0.9])
plot(1:7,nanstd(CmeansU),'Color',[0.9 0 0.9])
plot(1:7,nanstd(HeirmeansU),'Color',[0 0.9 0.9])

hold off


%% Load Wells BINARY TRANSFORM FIRST

Samp = 400;
indsX = [];

[~, TCis, ~, GI, RC, gene_names] = cross_validate('tracked_cells1',2000,indsX,Samp);

U = zeros(7,100);

KmeansU = zeros(100,7);
GMMmeansU = zeros(100,7);
CmeansU = zeros(100,7);
HeirmeansU = zeros(100,7);

% Mean DBI for shape clustering methods with continuous features over 100 resamplings

for k = 1:7
    for i = 1:100
        i
        [Smat,TCs] = sample(TCis,300,2000);

        TCnum = numel(TCs);
        Feats = size(TCs{1},2);
        
        [Sbin,TCbin] = binDataMean(Smat,TCs);

        arg = {Sbin,TCbin,300,GI,RC,k};
        
        KmeansU(i,k) = shapeClusterKmeans(arg);    
        CmeansU(i,k) = shapeClusterCmeans(arg);
        HeirmeansU(i,k) = shapeClusterHeirarchichal(arg);
       
    end
end

%%
hold on

plot(1:7,nanmean(PCMeanU'),'Color',[0.9 0 0 ])
plot(1:7,nanmean(KmeansU),'Color',[0 0.9 0 ])
plot(1:7,nanmean(CmeansU),'Color',[0.9 0 0.9])
plot(1:7,nanmean(HeirmeansU),'Color',[0 0.9 0.9])

hold off
%%


hold on

plot(1:7,median(PCMeanU'),'Color',[0.9 0 0 ])
plot(1:7,median(KmeansU),'Color',[0 0.9 0 ])
plot(1:7,median(GMMmeansU),'Color',[0 0 0.9])
plot(1:7,median(CmeansU),'Color',[0.9 0 0.9])
plot(1:7,median(HeirmeansU),'Color',[0 0.9 0.9])

hold off

%%

hold on

plot(1:7,nanstd(PCMeanU'),'Color',[0.9 0 0 ])
plot(1:7,nanstd(KmeansU),'Color',[0 0.9 0 ])
plot(1:7,nanstd(CmeansU),'Color',[0.9 0 0.9])
plot(1:7,nanstd(HeirmeansU),'Color',[0 0.9 0.9])

hold off

