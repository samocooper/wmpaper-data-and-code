% script for determining shape cluster number in an unsupervised manner

Samp = 400;
indsX = [];

[~, TCis, ~, GI, RC, gene_names] = cross_validate('tracked_cells1',2000,indsX,Samp);

Cs = cell(10,100);
KmeansU = zeros(10,100);


%% Generate centroid matrix for different k
for k = 1:8
    for i = 1:100
        i
        [Smat,TCs] = sample(TCis,300,2000);

        TCnum = numel(TCs);
        Feats = size(TCs{1},2);
        
        [Sbin,TCbin] = binDataMean(Smat,TCs);

        arg = {Sbin,TCbin,300,GI,RC,k};
        
        [KmeansU(i,k), Cs{i,k}] = shapeClusterKmeans(arg);          
       
    end
end

%% Which Clusters Map to Themselves uses k centroids mat from above

Cpc = [];
Id = zeros(10,10);
U = [];
CMmean = {};
for k = 1:8
    k
    for i = 1:100
        Cpc(((i-1)*k)+1:i*k,:) = Cs{i,k}(:,:);
    end

    for i = 1:50

            [cidx,Cmean] = kmeans(Cpc,k); 
            sil = silhouette(Cpc,cidx);
            CMmean{k,i} = Cmean;
            U(k,i) = mean(sil);
            
            %[~,PCs] = princomp(Cpc);
            %scatter3(Cpc(:,1),Cpc(:,2),Cpc(:,3),[],cidx);        
    end
end
%% plot results
[ID,ind] = max(U,[],2);
plot(ID)
Cbest = CMmean{2,ind};


