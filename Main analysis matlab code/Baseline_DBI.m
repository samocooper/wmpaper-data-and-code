% Script for generating baseline DBI values based of PCA

%% Load Wells

Samp = 400;
indsX = [];

[~, TCis, ~, GI, RC, gene_names] = cross_validate('tracked_cells1',2000,indsX,Samp);

U = zeros(1,100);

% Mean DBI for well means over 100 resamplings

for i = 1:100

    [~,TCs] = sample(TCis,300,2000);

    TCnum = numel(TCs);
    Feats = size(TCs{1},2);
    Mwell = zeros(TCnum,Feats);

    for j = 1:TCnum 
        Mwell(j,:) = mean(TCs{j});    
    end
    U(i) = DBcost(Mwell,GI,RC);
end
%%
GNU = unique(gene_names);
M3Dcol = [];
for i = 1:numel(GNU)
    M3Dcol(i,:) = mean(Mwell(GI == i,:));    
end



%%
mean(U)
std(U)

%% Same with PCA first

Samp = 400;
indsX = [];

[~, TCis, ~, GI, RC, gene_names] = cross_validate('tracked_cells1',2000,indsX,Samp);

U = zeros(1,100);

for k = 1:7
    for i = 1:100

        [~,TCs] = sample(TCis,300,2000);

        TCnum = numel(TCs);
        Feats = size(TCs{1},2);
        Mwell = zeros(TCnum,Feats);

        for j = 1:TCnum 
            Mwell(j,:) = mean(TCs{j});    
        end

        [~,Mwell] = princomp(Mwell);   

        U(k,i) = DBcost(Mwell(:,1:k),GI,RC);
    end
end

mean(U')
std(U')

%%
PCMeanU = U;




