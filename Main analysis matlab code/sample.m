function [Smat, TCs] = sample(TCsI,TCSnum,Snum)

TCnum = numel(TCsI);
Ssamp = round(Snum/TCnum);
Smat = [];
TCs = cell(1,TCnum);

for i = 1:TCnum
    temp = TCsI{i};
    temp = temp(randperm(size(temp,1)),:);
    TCs{i} = temp(1:TCSnum,:);

    j = ((i-1)*Ssamp)+1;
    Smat(j:j+Ssamp,:) = temp(1:Ssamp+1,:);
end