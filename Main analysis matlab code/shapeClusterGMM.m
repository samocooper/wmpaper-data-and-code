function cost = shapeClusterGMM(arg)

    Smat = arg{1};
    TCs = arg{2};
    TCsize = arg{3};
    GI = arg{4};
    RC = arg{5};    
    k = arg{6};
    
    obj = gmdistribution.fit(Smat,k);
    
    Dshape = zeros(numel(GI),k);

    for i = 1:numel(GI)
        
        index_tc = cluster(obj,TCs{i});

        for j = 1:k
            Dshape(i,j) = sum(index_tc == j);
        end
    end
    
    Dnorm = Dshape./TCsize;
    [~,I] = sort(Dnorm(1,:),2);
    Dnorm = Dnorm(:,I);   
    cost = DBcost(Dnorm,GI,RC);
end