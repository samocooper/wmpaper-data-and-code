% ---- Find files define arrays matrices etc. Need best clusters and dist_normalised---- %THIS FILE FIND IS OUTDATED
% VERSION BUT HAS EXTRA INFO NEEDED FOR DYNAMIC ANALYSIS
warning('off','stats:kmeans:EmptyClusterRep')
names = dir('tracked_cells1');

fnum = length(names);
fnames = {};
fcount = 0;
prev_gene = [];
gene_index = [];
gene_count = 0;
gene_names = {};

% ---- Link gene knockdowns to matrices ----

for i = 1:fnum
    if strfind(names(i).name,'csv')>0        
        fcount = fcount+1;
        file_path = strcat('tracked_cells1/',names(i).name);
        fnames{fcount} = file_path;
        a = strfind(names(i).name,'_');
        gene = names(i).name(1:a-1);
        gene_names{fcount} = gene;
        if strcmp(gene,prev_gene)
            gene_index(fcount)= gene_count;            
        else
            prev_gene = gene;
            gene_count = gene_count + 1;            
            gene_index(fcount)= gene_count;
        end
                    
    end
end
%%
Samp = 400;
indsX = [];
[Smat, TCis, ~, GI, RC, gene_names,mu,sigma] = cross_validate('tracked_cells1',2000,indsX,Samp);
temp_means = mean(Smat);

%% 

k = 6;
shape_change = zeros(k);
tc_dynamics = zeros(fcount,k^2);

Mat_dists = zeros(k,k,fcount);
all_Dmean = [];
all_TCmean = [];
for h = 1:fcount
    h
    temp_shape_change = zeros(k);
    fid = fopen(fnames{h});
    tcells = {};
    fline = fgetl(fid);
    tcell = str2num(fline(1:end-1));
    i = 0;
    
    Dmean = zeros(1,numel(temp_means));
    TCmean = zeros(1,numel(temp_means));

    while feof(fid) == false
        fline = fgetl(fid);
        if isempty(fline)
             i = i+ 1;
             tcells{i} = tcell;
             tcell = [];
        else
            temp_tcell = str2num(fline(1:end-1));
            tcell = [tcell; temp_tcell];
        end
    end
    cell_count = length(tcells);
    all_count = 0;
    for i = 1:cell_count
        [time_points,f] = size(tcells{i});
        all_count = all_count + time_points;
        tc_mean_mat = repmat(temp_means,time_points,1);
        temp_mu = repmat(mu,time_points,1);
        temp_sigma = repmat(sigma,time_points,1);

        % ---- Convert cells to binary values ----
        norm_cell = (tcells{i}-temp_mu)./temp_sigma;
        clustered_cell = norm_cell > tc_mean_mat;
        clustered_cell = +clustered_cell;
        
        % ---- Asign to a shape cluster ----
        
        [dist_tc,index_tc] = min(pdist2(clustered_cell,Cbest),[],2);
        
        % ---- Has shaped changed since the last timepoint ----        
        
        for j = 1:time_points-1           
            temp_shape_change(index_tc(j),index_tc(j+1)) = temp_shape_change(index_tc(j),index_tc(j+1)) + 1;
        end
        
        % ---- Mean change in feature values ----
        for j = 2:time_points
            Dmean = Dmean + abs(norm_cell(j,:) - norm_cell(j-1,:));
        end
        TCmean = TCmean + mean(norm_cell);
      
    end
    temp_shape_dist_mat = repmat(Dnorm(h,:),k,1)';
    norm_shape_change = temp_shape_change./temp_shape_dist_mat;
    all_Dmean(h,:) = Dmean./all_count;
    all_TCmean(h,:) = TCmean./all_count;
    Mat_dists(:,:,h) = temp_shape_change;
end
%% ---- Calculate PC graph here and plot here for supplementar as dynamic FVs can easily be obtained----
g_Dmean = [];
g_TCmean = [];
for i = 1:max(GI)
    g_Dmean(i,:) = mean(all_Dmean(GI==i,:));
    g_TCmean(i,:) = mean(all_TCmean(GI==i,:));
end

[W,A,~,~,L] = pca(g_TCmean);
[~,B] = pca(g_TCmean);

%[W,A,~,~,L] = pca([all_Dmean all_TCmean]);
%DBcost(A(:,1:6),GI,RC)
bar(L(1:10))
L(2)
%%

x = A(:,1);
y = A(:,2);
scatter(x,y,100,B(:,1),'fill');

corr(A(:,1),B(:,1))

dx = 0;
dy = 0.015;

text(x-dx,y-dy,gene_names,'FontSize',12);


%% DBI of dynamics and hierarchical cluster of all dynamic matrices
Z = [];
for i = 1:52
   temp = reshape(Mat_dists(:,:,i),1,[]);
   Z(i,:) = temp;
end
[~,Z,~,~,L] = pca(Z);
DBcost(Z(:,1:6),GI,RC)

labels = {};
for i = 1:numel(GI)
    labels{i} = gene_names{GI(i)};
end

Y = pdist(Z);
squareform(Y);
Z3 = linkage(Y,'ward');
[H,T,outperm] = dendrogram(Z3,0,'Labels',labels,'Orientation','right');
%% What is the variance explained by componnets after dynamics are included?
bar(L(1:10))


Avg_mat_dists = zeros(k,k,max(GI));
for i = 1:max(GI)
    temp = zeros(k,k);
    temp(:,:) = mean(Mat_dists(:,:,GI == i),3);
    Avg_mat_dists(:,:,i) = temp;    
end
%% Number of transtions against shape 'distance' i.e. majority made to neighbouring shapes
Z = mean(Avg_mat_dists,3);
D = zeros(6);
for i = 1:6
    for j = 1:6
        D(i,j) = abs(i-j);
    end
end


scatter(reshape(D,1,[]),reshape(Z,1,[]),60)
%% Hierarchichal clustering of averaged dynamc mats

Z1 = [];

for i = 1:18
   temp = reshape(Avg_mat_dists(:,:,i),1,[]);
   Z1(i,:) = temp;
end
Z1 = zscore(Z1');
Y = pdist(Z1');
squareform(Y);
Z2 = linkage(Y,'ward');
[H,T,outperm] = dendrogram(Z2,0,'Labels',gene_names,'Orientation','right');

%%
ident = zeros(size(Avg_mat_dists));
for i = 1:18
    ident(:,:,i) = eye(6,6);
end

diag = ident.*Avg_mat_dists;
off_diag = (ones(size(Avg_mat_dists))-ident).*Avg_mat_dists;
diag = sum(sum(diag),2);
off_diag = sum(sum(off_diag),2);
dynamic_measure = off_diag(:)./diag(:);
[dynamic_measure_sorted, Ind] = sort(dynamic_measure);

% ---- Plot shape transition matrices ----
for i = 1:18
    subplot(4,5,i)
    temp = zeros(k,k);
    temp(:,:) = log(Avg_mat_dists(:,:,outperm(i)));
    imagesc(temp)
    colormap(cool)
    temp_val = num2str(dynamic_measure(outperm(i)),3);    
     ti = strcat(gene_names{outperm(i)},{' '},temp_val);
    title(ti)
    set(gca, 'YTickLabelMode', 'manual', 'YTickLabel', []);
    set(gca, 'XTickLabelMode', 'manual', 'XTickLabel', []);
    %shape_change = shape_change + norm_shape_change;
    %tc_dynamics(h,:) = reshape(norm_shape_change,1,[]);    
end

