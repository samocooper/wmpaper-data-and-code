% Script for generating distirbution of shapes, feature centers, clustering
% of static SCPs and PCA of feature centers

Samp = 400;
indsX = [];

[Smat, TCis, ~, GI, RC, gene_names,mu,sigma] = cross_validate('tracked_cells1',2000,indsX,Samp);


k= 6;
U = [];


TCnum = numel(TCis);
Feats = size(TCis{1},2);

[Sbin,TCbin] = binDataMean(Smat,TCis);

arg = {Sbin,TCbin,400,GI,RC,k};

[cost,Dnorm] = shapeClusterC(arg,Cbest);

%% Distribution of shapes for all wells

bar(Dnorm,'stacked')
%% Average over wells and cluster
Dmean = [];
for i = 1:max(GI)
    Dmean(i,:) = mean(Dnorm(GI==i,:));
end
labels = {};
for i = 1:numel(GI)
    labels{i} = gene_names{GI(i)};
end
Y = pdist(Dmean);
squareform(Y);
Z = linkage(Y,'ward');
figure
[H,T,outperm] = dendrogram(Z,0,'labels',gene_names,'Orientation','right');
set(H,'Color',[0 0 0])

%% shape distribution averaged over wells with same order as clustering above
figure
imagesc(Dmean(outperm,:));
gene_names(outperm);

Dmean1 = Dmean(outperm,:);
gene_names1 = gene_names(outperm);

M = mean(Dmean1);
M = repmat(M,size(Dmean1,1),1);
DmeanN = Dmean1-M;
gene_names1

gene_names{1} = 'Rnd2';
gene_names{12} = 'Rnd3';

%% What are cluster mean values for each feature

[~,index_Smat] = min(pdist2(Sbin,Cbest),[],2);
Cmean = [];
for i = 1:6
    Cmean(i,:) = mean(Smat(index_Smat==i,:));
    Cstd(i,:) = std(Smat(index_Smat==i,:));
end
figure
imagesc(Cmean)
%% Perform PCA on pooled sample matrix what variance do PC1 and PC2 explain
[~,Spc,~,~,L] = pca(Smat);
CmeanPC = [];
CstdPC = [];
for i = 1:6
    CmeanPC(i,:) = mean(Spc(index_Smat==i,:));
    CstdPC(i,:) = std(Spc(index_Smat==i,:));
end
L(1) + L(2)
%% Plot mean and std deviations as normal distirbutions scaled to membership

CmeanPC(:,1:2)
CstdPC(:,1:2)

x1 = -8:.4:8; x2 = -8:.4:8;
[X1,X2] = meshgrid(x1,x2);
F = zeros(numel(X1),1);
for i = 1:6
    mu = [CmeanPC(i,1) CmeanPC(i,2)];
    Sigma = [CstdPC(i,1) 0; 0 CstdPC(i,2)];
    F = F + mvnpdf([X1(:) X2(:)],mu,Sigma)*Dmean1(1,i);
end
figure
F = reshape(F,length(x2),length(x1));
surf(x1,x2,F);
axis([-8 8 -8 8 0 .1])
xlabel('x1'); ylabel('x2'); zlabel('Probability Density');



