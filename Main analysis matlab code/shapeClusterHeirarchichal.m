function cost = shapeClusterKmeansHeirarchichal(arg)

    Smat = arg{1};
    TCs = arg{2};
    TCsize = arg{3};
    GI = arg{4};
    RC = arg{5};    
    k = arg{6};
    
    Y = pdist(Smat);
    Z = linkage(Y,'ward');
    T = cluster(Z,'maxclust',k);
    
    Dshape = zeros(numel(GI),k);

    for i = 1:numel(GI)
        
        index_tc = classify(TCs{i},Smat,T);

        for j = 1:k
            Dshape(i,j) = sum(index_tc == j);
        end
    end
    
    Dnorm = Dshape./TCsize;
    cost = DBcost(Dnorm,GI,RC);
end