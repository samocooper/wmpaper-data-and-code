function dist = daviesBouldin(C,S)

    k = size(C,1);
    
    Smat = repmat(S,k,1);
    Smat = Smat + Smat';
    
    C = C + eye(k);
    Sqr = Smat./C;
    Sqr(logical(eye(k))) = 0;
    
    A = max(Sqr,[],1);
    dist = mean(A);    
end