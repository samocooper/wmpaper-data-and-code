function [MW,C] = cmeans(X,k,m,MaxIter)

%Initialise

[rows,cols] = size(X);

r  = ceil(rand(1,k)*rows);
C = X(r,:)+rand(k,cols)*0.0001;

%Main loop

for i = 1:MaxIter
    
    
    MW = membership(X,C,k,m);    
    MW=MW.^m;
    
    MW1 = repmat(MW,1,1,cols);

    X1 = repmat(X,1,1,k);
    X1 = permute(X1,[1 3 2]);

    Mn = MW1.*X1;    
    
    Ct =sum(Mn,1)./sum(MW1,1);
    C(:,:) = Ct(1,:,:);

end
