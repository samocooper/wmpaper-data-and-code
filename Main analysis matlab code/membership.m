function MW = membership(X,C,k,m)

    MCB = pdist2(X,C,'cityblock');   
    MCB1 = repmat(MCB,1,1,k);
    MCB2 = permute(MCB1,[1 3 2]);
    
    a = (2/(m-1));
    
    MW = (MCB1./MCB2);    
    MW = realpow(MW,a);
    MW = 1./sum(MW,3);

end