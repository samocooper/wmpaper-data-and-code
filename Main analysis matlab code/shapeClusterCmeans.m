function [cost,C] = shapeClusterCmeans(arg)

    Smat = arg{1};
    TCs = arg{2};
    TCsize = arg{3};
    GI = arg{4};
    RC = arg{5};    
    k = arg{6};
    
    [~,C] = cmeans(Smat,k,1.5,100);
    
    Dshape = zeros(numel(GI),k);

    for i = 1:numel(GI)
        
        [~,index_tc] = min(pdist2(TCs{i},C),[],2);

        for j = 1:k
            Dshape(i,j) = sum(index_tc == j);
        end
    end
    
    Dnorm = Dshape./TCsize;
    [~,I] = sort(Dnorm(1,:),2);
    Dnorm = Dnorm(:,I);
    cost = DBcost(Dnorm,GI,RC);
end