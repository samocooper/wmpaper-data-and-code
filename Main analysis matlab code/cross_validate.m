function [Smat, TCs, TCsize, GI, RC, gene_names,mu,sigma] = cross_validate(Dname,Asize,inds,TCsamp)

Dnames = dir(Dname);

Fnum = length(Dnames);
Fcount = 0;

prev_gene = [];
GI = [];
g = 0;
gene_names = {};
%% Identify CSV files: name format is X_Y where X is class and Y some identifier

for i = 1:Fnum
    if strfind(Dnames(i).name,'csv')>0
        
        Fcount = Fcount+1;
        file_path = strcat('tracked_cells1/',Dnames(i).name);
        
        Fnamest{Fcount} = file_path;
        a = strfind(Dnames(i).name,'_');
        
        gene = Dnames(i).name(1:a-1);
        
        if strcmp(gene,prev_gene)
            GIt(Fcount)= g;
        else
            g = g + 1;
            gene_names{g} = gene;
            prev_gene = gene;
            GIt(Fcount)= g;
        end
        
    end
    
end

gene_names(inds) = [];

count = 0;
GI = [];
FnamesT = {};
for i = 1:g
    if ~sum(i == inds)
        count = count+1;
        temp = ones(1,sum(GIt == i));
        GI = [GI temp*count];                
        for j = 1:numel(temp)
            z = {Fnamest{GIt == i}};
            FnamesT{end+1} = z{j};
        end
    end
end
Fcount = numel(FnamesT);

%number of repeats

RC = zeros(1,count);

for i = 1:count
    RC(i) = sum(GI == i);
end

%R = randperm(Fcount);
for i = 1:Fcount
    Fnames{i} = FnamesT{i};
end
%% Create a matrix sampled from all TCs, zScore Sample mat and normalise TCs to this

TCs = cell(1,Fcount);
TCsize = zeros(1,Fcount);

Fcount = int32(Fcount);
Asize = int32(Asize);


Samp = Asize/Fcount;
Smat = [];


% load tcs from files

for i = 1:Fcount
    tc = csvread(Fnames{i});
    
    tc = tc(1:400,1:end-1);
    tc = tc(randperm(400),:);
    
    tc = tc(1:TCsamp,:);
    
    j = ((i-1)*Samp)+1;
    Smat(j:j+Samp,:) = tc(1:Samp+1,:);
    
    TCs{i} = tc;
    
end

[Smat,mu,sigma] = zscore(Smat);

for i = 1:Fcount
    
    rows = size(TCs{i},1);
    TCsize(i) = rows;
    
    for j = 1:rows
        TCs{i}(j,:) = (TCs{i}(j,:)-mu)./sigma;
    end
end
